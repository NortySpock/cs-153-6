//////////////////////////////////////////////////////////////////////////////
/// @file btpreorderiterator.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the post-order iterator implementation file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator ()
/// @brief Pre-order iterator constructor.
/// @pre Pre-order iterator does not exist
/// @post Pre-order iterator exists with iterator position not set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPreorderIterator<generic>::BTPreorderIterator ()
{
  m_current = NULL;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator (Btn<generic> *)
/// @brief Pre-order iterator constructor.
/// @pre Pre-order iterator does not exist
/// @post Pre-order iterator exists with iterator position already set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPreorderIterator<generic>::BTPreorderIterator (Btn<generic> * x) : m_current (x)
{
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Dereference operator for Pre-order iterator.
/// @pre Iterator is pointing at a non-NULL node of tree.
/// @post Iterator is pointing at a non-NULL node of tree.
/// @param None.
/// @return Data pointed to by node of tree.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic BTPreorderIterator<generic>::operator* () const
{
  return *(m_current -> data);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator operator++ ()
/// @brief Iterate operator for iterator. Steps one step through the tree.
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPreorderIterator<generic> BTPreorderIterator<generic>::operator++ ()
{
  if(m_current !=NULL)
  {
    //We can go left 
    if(m_current -> l != NULL)
    {
      //Go left
      m_current = m_current -> l;
    }
    else if(m_current -> r != NULL)
    {
      //Go left
      m_current = m_current -> r;
    }
    else//Hit bottom, need to go up
    {
      if(m_current -> p != NULL)
      {
        if(m_current -> p -> l == m_current)//If left child
        {
          //While we can go up AND (we can't branch over OR we're coming up from the right) Og this is ugly!
          while(m_current -> p != NULL && ((m_current -> p -> l == NULL || m_current -> p -> r == NULL) || m_current -> p -> r == m_current))
          {
            m_current = m_current -> p;
          }
          //We can't branch over, so we hit the top and have nowhere to go
          if(m_current -> p == NULL)
          {
            m_current = NULL;
          }
          //So long as we didn't just slam our head into the roof, we can branch over
          if(m_current != NULL && m_current -> p != NULL && m_current -> p -> l != NULL && m_current -> p -> r != NULL)
          {
            m_current = m_current -> p -> r; //Go to right child
          }
        }
        else if(m_current -> p -> r == m_current)//If right child
        {
          //While can go up and right child OR doesn't branch (Og this is ugly too!)
          while(m_current -> p != NULL && (m_current -> p -> r == m_current || (m_current -> p -> l == NULL || m_current -> p -> r == NULL)))
          {
            m_current = m_current -> p;//go up
          }
          if(m_current -> p != NULL)//We ran into top.
          {
            m_current = NULL;
          }
        }
      }
      else//We are at root and want to move up, so done.
      {
        m_current = NULL;
      }
    }
  }

  return * this;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator operator++ (int)
/// @brief Altered version of the normal ++ operator to handle preincrements
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPreorderIterator<generic> BTPreorderIterator<generic>::operator++ (int)
{
  return ++(*this);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const BTPreorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: true if iterators are pointing at the same thing, false if not
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool BTPreorderIterator<generic>::operator== (const BTPreorderIterator & x) const
{
  return m_current == x.m_current;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const BTPreorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: false if iterators are pointing at the same thing, true if not
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool BTPreorderIterator<generic>::operator!= (const BTPreorderIterator & x) const
{
  return m_current != x.m_current;
}

