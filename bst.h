//////////////////////////////////////////////////////////////////////////////
/// @file bst.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the bst header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class Bst
/// @brief The Binary Search Tree, which inheirits some properties of the 
/// Binary Tree class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void insert (generic)
/// @brief Inserts an element into the tree. If the element already exists in
/// the tree, this function does nothing. (You should not have multiple 
/// elements of the same value in the tree.) 
/// @pre Tree exists
/// @post Tree has one more unique element in it.
/// @param Element to be inserted
/// @return Nothing (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void remove (generic)
/// @brief Removes an element in the tree, if it exists
/// @pre Tree contains elements (at least one)
/// NOTE: If tree is empty, returns exception.
/// @post Tree does not contain the element specified.
/// @param Element to be removed.
/// @return Nothing (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty ()
/// @brief Returns whether or not the tree is empty 
/// (It does this by checking if m_root is null)
/// @pre Tree exists
/// @post No change
/// @param None
/// @return True if tree is empty, false if tree is not.
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size ()
/// @brief Returns the current number of elements in the tree
/// @pre Tree exists
/// @post No change
/// @param None
/// @return Number of elements in the tree.
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn PreOrder pre_search (generic)
/// @brief This returns the PreOrder iterator
/// starting at the node containing the indicated <generic>
/// @pre Tree and iterator exist
/// @post Iterator now at <generic>
/// @param <generic>
/// @return PreOrder Iterator at <generic>
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn InOrder in_search (generic)
/// @brief This returns the InOrder iterator
/// starting at the node containing the indicated <generic>
/// @pre Tree and iterator exist
/// @post Iterator now at <generic>
/// @param <generic>
/// @return InOrder Iterator at <generic>
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn PostOrder post_search (generic)
/// @brief This returns the PostOrder iterator
/// starting at the node containing the indicated <generic>
/// @pre Tree and iterator exist
/// @post Iterator now at <generic>
/// @param <generic>
/// @return PostOrder Iterator at <generic>
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void p_insert (generic)
/// @brief Inserts an element into the tree. If the element already exists in
/// the tree, this function does nothing. (You should not have multiple 
/// elements of the same value in the tree.) 
/// @pre Tree exists
/// @post Tree has one more unique element in it.
/// @param Element to be inserted
/// @return pointer to parent of inserted
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void p_remove (generic)
/// @brief Removes an element in the tree, if it exists
/// @pre Tree contains elements (at least one)
/// NOTE: If tree is empty, returns exception.
/// @post Tree does not contain the element specified.
/// @param Element to be removed.
/// @return pointer to parent of removed node
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void p_search (generic)
/// @brief Find parent of node searched for
/// @pre Tree contains elements (at least one)
/// NOTE: If tree is empty, returns exception.
/// @post No change.
/// @param Element to be found.
/// @return Pointer to parent of found node
////////////////////////////////////////////////////////////////////////////// 

#ifndef BST_H
#define BST_H

#include "bt.h"
#include "btn.h"
#include "exception.h"

template <class generic>
class Bst : public BT<generic>
{
  public:
    Bst();
    ~Bst();
    void insert (generic);
    void remove (generic);
    bool empty();
    unsigned int size();

    typedef BTPreorderIterator<generic> PreOrder;
    typedef BTInorderIterator<generic> InOrder;
    typedef BTPostorderIterator<generic> PostOrder;
    PreOrder pre_search (generic);
    InOrder in_search (generic);
    PostOrder post_search (generic);
    generic & search(generic);
    Btn<generic> * searchptr(generic);

  protected:
    Btn<generic> * p_insert (generic); //returns a pointer to the parent of the inserted node
    Btn<generic> * p_remove (generic); //returns a pointer to the parent of the deleted node
    Btn<generic> * p_search (generic); //returns a pointer to the parent of the node containing the found data
};

#include "bst.hpp"
#endif

