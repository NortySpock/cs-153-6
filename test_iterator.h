//////////////////////////////////////////////////////////////////////////////
/// @file test_iterator.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header file for the test of the iterators.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class test_iterator
/// @brief This is the header file for the class that tests the iterators
//////////////////////////////////////////////////////////////////////////////


#ifndef TEST_ITERATOR_H
#define TEST_ITERATOR_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "bt.h"
#include "bst.h"
#include "btinorderiterator.h"

class Test_iterator : public CPPUNIT_NS::TestFixture
{
  private:
  CPPUNIT_TEST_SUITE (Test_iterator);
     CPPUNIT_TEST (test_PreOrder_constructor);
     CPPUNIT_TEST (test_PreOrder_copy_constructor);
    CPPUNIT_TEST (test_PreOrder_dereference);
    CPPUNIT_TEST (test_PreOrder_iterator);
    CPPUNIT_TEST (test_PreOrder_equal);
    CPPUNIT_TEST (test_PreOrder_not_equal);
    CPPUNIT_TEST (test_PreOrder_search);

    CPPUNIT_TEST (test_InOrder_constructor);
    CPPUNIT_TEST (test_InOrder_copy_constructor);
    CPPUNIT_TEST (test_InOrder_dereference);
    CPPUNIT_TEST (test_InOrder_iterator);
    CPPUNIT_TEST (test_InOrder_equal);
    CPPUNIT_TEST (test_InOrder_not_equal);
    CPPUNIT_TEST (test_InOrder_search);
    CPPUNIT_TEST (test_InOrder_hammer);


     CPPUNIT_TEST (test_PostOrder_constructor);
     CPPUNIT_TEST (test_PostOrder_copy_constructor);
     CPPUNIT_TEST (test_PostOrder_iterator);
    CPPUNIT_TEST (test_PostOrder_dereference);
    CPPUNIT_TEST (test_PostOrder_equal);
    CPPUNIT_TEST (test_PostOrder_not_equal);  
    CPPUNIT_TEST (test_PostOrder_search); 
  CPPUNIT_TEST_SUITE_END ();

  protected:
  void test_PreOrder_constructor();
  void test_PreOrder_copy_constructor();
  void test_PreOrder_dereference();
  void test_PreOrder_iterator();
  void test_PreOrder_equal();
  void test_PreOrder_not_equal();
  void test_PreOrder_search();
  
  void test_InOrder_constructor();
  void test_InOrder_copy_constructor();
  void test_InOrder_dereference();
  void test_InOrder_iterator();
  void test_InOrder_hammer();
  void test_InOrder_equal();
  void test_InOrder_not_equal();
  void test_InOrder_search();

  void test_PostOrder_constructor();  
  void test_PostOrder_copy_constructor();
  void test_PostOrder_iterator();
  void test_PostOrder_dereference();
  void test_PostOrder_equal();
  void test_PostOrder_not_equal();
  void test_PostOrder_search();
};

#endif

