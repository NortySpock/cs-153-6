//////////////////////////////////////////////////////////////////////////////
/// @file btinorderiterator.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the in-order iterator header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class btinorderiterator
/// @brief The in order iterator for a binary tree.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTInorderIterator ()
/// @brief In-order iterator constructor.
/// @pre In-order iterator does not exist
/// @post In-order iterator exists with iterator position not set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTInorderIterator (Btn<generic> *)
/// @brief In-order iterator constructor.
/// @pre In-order iterator does not exist
/// @post In-order iterator exists with iterator position already set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Dereference operator for In-order iterator.
/// @pre Iterator is pointing at a non-NULL node of tree.
/// @post Iterator is pointing at a non-NULL node of tree.
/// @param None.
/// @return Data pointed to by node of tree.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTInorderIterator operator++ ()
/// @brief Iterate operator for iterator. Steps one step through the tree.
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTInorderIterator operator++ (int)
/// @brief Altered version of the normal ++ operator to handle preincrements
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const BTInorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: true if iterators are pointing at the same thing, false if not
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const BTInorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: false if iterators are pointing at the same thing, true if not
//////////////////////////////////////////////////////////////////////////////

#ifndef BTINORDERITERATOR_H
#define BTINORDERITERATOR_H

#include "bt.h"

template <class generic>
class BTInorderIterator
{
  public:
    BTInorderIterator ();
    BTInorderIterator (Btn<generic> * x);
    generic operator* () const;
    BTInorderIterator operator++ ();
    BTInorderIterator operator++ (int);//To handle ++x
    bool operator== (const BTInorderIterator &) const;
    bool operator!= (const BTInorderIterator &) const;
    
  private:
    Btn <generic> * m_current;//The node the iterator is currently pointing at
};

#include "btinorderiterator.hpp"
#endif

