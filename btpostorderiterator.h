//////////////////////////////////////////////////////////////////////////////
/// @file btpostorderiterator.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the post-order iterator header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class btpostorderiterator
/// @brief The post-order iterator for a binary tree.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPostorderIterator ()
/// @brief Post-order iterator constructor.
/// @pre Post-order iterator does not exist
/// @post Post-order iterator exists with iterator position not set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPostorderIterator (Btn<generic> *)
/// @brief Post-order iterator constructor.
/// @pre Post-order iterator does not exist
/// @post In-order iterator exists with iterator position already set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Dereference operator for Post-order iterator.
/// @pre Iterator is pointing at a non-NULL node of tree.
/// @post Iterator is pointing at a non-NULL node of tree.
/// @param None.
/// @return Data pointed to by node of tree.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPostorderIterator operator++ ()
/// @brief Iterate operator for iterator. Steps one step through the tree.
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPostorderIterator operator++ (int)
/// @brief Altered version of the normal ++ operator to handle preincrements
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const BTPostorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: true if iterators are pointing at the same thing, false if not
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const BTPostorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: false if iterators are pointing at the same thing, true if not
//////////////////////////////////////////////////////////////////////////////

#ifndef BTPOSTORDERITERATOR_H
#define BTPOSTORDERITERATOR_H

#include "btn.h"

template <class generic>
class BTPostorderIterator
{
  public:
    BTPostorderIterator (); //constructor
    BTPostorderIterator (Btn<generic> *); //Sets iterator position to current pointer?
    generic operator* () const;
    BTPostorderIterator operator++ ();
    BTPostorderIterator operator++ (int);
    bool operator== (const BTPostorderIterator &) const;
    bool operator!= (const BTPostorderIterator &) const;
    
  private:
    Btn<generic> * m_current;//The node the iterator is currently pointing at
    Btn<generic> * m_previous;//The node the iterator just pointed at.
};

#include "btpostorderiterator.hpp"
#endif

