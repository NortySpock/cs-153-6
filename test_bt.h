//////////////////////////////////////////////////////////////////////////////
/// @file test_bt.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header file for the test of the BT class.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class test_bt
/// @brief This is the header file for the class that tests the BT
//////////////////////////////////////////////////////////////////////////////

#ifndef TEST_BT_H
#define TEST_BT_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "bt.h"
#include "bst.h"

class Test_bt : public CPPUNIT_NS::TestFixture
{
  private:
  CPPUNIT_TEST_SUITE (Test_bt);
    CPPUNIT_TEST (test_constructor);
    CPPUNIT_TEST (test_copy_constructor);
    CPPUNIT_TEST (test_assignment);
    CPPUNIT_TEST (test_clear);
    CPPUNIT_TEST (test_empty);
    CPPUNIT_TEST (test_size);
  CPPUNIT_TEST_SUITE_END ();

  protected:
  void test_constructor();
  void test_copy_constructor();
  void test_assignment();
  void test_clear();
  void test_empty();
  void test_size();

};

#endif

