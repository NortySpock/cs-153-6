//////////////////////////////////////////////////////////////////////////////
/// @file test_bst.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the test of the BST class.
//////////////////////////////////////////////////////////////////////////////

#include "test_bst.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
//#include <ctime>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_bst);

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor();
/// @brief This tests the constructor of the BST class
/// @pre BST class should already be declared. 
/// This function will instantiate a BST and make sure the initial 
/// values are correct (size is zero, m_root is null).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_constructor()
{  
	srand(time(NULL));
	Bst <int> a;
	CPPUNIT_ASSERT(a.size() == 0);
	CPPUNIT_ASSERT(a.empty() == true);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_insert();
/// @brief This tests the insert function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the insert function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_insert()
{ 
	//Insert at root
	Bst <int> a; 
	a.insert(42);
	CPPUNIT_ASSERT(a.size() == 1);
	CPPUNIT_ASSERT(a.search(42) == 42);
	CPPUNIT_ASSERT(!a.empty());
	
	//Insert less than
	a.insert(40);
	CPPUNIT_ASSERT(a.size() == 2);
	CPPUNIT_ASSERT(a.search(40) == 40);
	
	//Insert greater than
	a.insert(45);
	CPPUNIT_ASSERT(a.size() == 3);
	CPPUNIT_ASSERT(a.search(45) == 45);

	//Insert prexisting
	a.insert(42);
	CPPUNIT_ASSERT(a.size() == 3);//No change in size
	CPPUNIT_ASSERT(a.search(42) == 42);//Still there.
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_remove();
/// @brief This tests the remove function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the remove function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_remove()
{

	//Test case one: no children
	Bst <int> a;
	CPPUNIT_ASSERT(a.empty());//See? It's empty
	
	a.insert(42);
	CPPUNIT_ASSERT(a.size() == 1);
	CPPUNIT_ASSERT(a.search(42) == 42);
	CPPUNIT_ASSERT(!a.empty());
	//Now I have a root element in it with no children.
	
	a.remove(42);//Now I delete it
	
	//And it's gone.
	CPPUNIT_ASSERT(a.size() == 0);
	CPPUNIT_ASSERT(a.empty());
	
	
	Bst <int> b;
	int test_b1[]={50, 75, 25, 20, 30, 60, 80}; //Three layer tree
	int test_b2[]={20, 30, 60, 80}; //The first layer
	int test_b3[]={25, 75}; //The second layer
	
	for(int i = 0; i < 7; i++)
	{
		b.insert(test_b1[i]);
	}
	
	for(int i = 0; i < 4; i++)
	{
		b.remove(test_b2[i]);
		CPPUNIT_ASSERT(b.size() == 7-1-i);
		CPPUNIT_ASSERT(b.searchptr(test_b2[i])==NULL);
	}
	
	for(int i = 0; i < 2; i++)
	{
		b.remove(test_b3[i]);
		CPPUNIT_ASSERT(b.size() == 2-i);
		CPPUNIT_ASSERT(b.searchptr(test_b3[i])==NULL);
	}
	
	b.remove(50);
	
	
	
	//Test case two: only one child
	//Test left side
	Bst <int> c;
	int test_c1[]={50, 40, 30, 20};
	
	for(int i = 0; i < 4; i++)
	{
		c.insert(test_c1[i]);
	}
	
	CPPUNIT_ASSERT(c.size() == 4);
	c.remove(40);
	CPPUNIT_ASSERT(c.searchptr(40)==NULL);
	CPPUNIT_ASSERT(c.size() == 3);
	
	c.remove(50);
	CPPUNIT_ASSERT(c.searchptr(50)==NULL);
	CPPUNIT_ASSERT(c.size() == 2);
	
	//Test right side
	Bst <int> d;
	int test_d1[]={50, 60, 70, 80};
	for(int i = 0; i < 4; i++)
	{
		d.insert(test_d1[i]);
	}
	
	CPPUNIT_ASSERT(d.size() == 4);
	d.remove(60);
	CPPUNIT_ASSERT(d.searchptr(60)==NULL);
	CPPUNIT_ASSERT(d.size() == 3);
	
	d.remove(50);
	CPPUNIT_ASSERT(d.searchptr(50)==NULL);
	CPPUNIT_ASSERT(d.size() == 2);
	
	
	//Test case four -- two children, no immediate left-right child.
	//Test root
	Bst <int> e;
	int test_e1[]={50, 75, 25, 15};
	
	for(int i = 0; i < 4; i++)
	{
		e.insert(test_e1[i]);
	}
	
	e.remove(50);
	CPPUNIT_ASSERT(e.searchptr(50)==NULL);
	CPPUNIT_ASSERT(e.size() == 3);
	
	//Test non-root
	Bst <int> f;
	int test_f1[]={50, 25, 15, 35};
	for(int i = 0; i < 4; i++)
	{
		f.insert(test_f1[i]);
	}
	
	f.remove(25);
	CPPUNIT_ASSERT(f.searchptr(25)==NULL);
	CPPUNIT_ASSERT(f.size() == 3);
	
	
	//Test case three -- two children, needs swap from left-rightmost child.
	Bst <int> g;
	int test_g1[]={50, 25, 75, 35};
	for(int i = 0; i < 4; i++)
	{
		g.insert(test_g1[i]);
	}
	
	g.remove(50);
	CPPUNIT_ASSERT(g.searchptr(50)==NULL);
	CPPUNIT_ASSERT(g.size()==3);
	
	
	//Test case five
	Bst <int> h;
	int test_h1[]={50, 25, 75, 35, 30};
	for(int i = 0; i < 5; i++)
	{
		h.insert(test_h1[i]);
	}
	
	h.remove(50);
	CPPUNIT_ASSERT(h.searchptr(50)==NULL);
	CPPUNIT_ASSERT(h.size()==4);
	
	//Demonstrate handling of remove case when item to be removed does not exist.
	h.remove(50);
	CPPUNIT_ASSERT(h.size()==4);//No change in size
	
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_search();
/// @brief This tests the search function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the search function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_search()
{
	Bst <int> a;
	
	try//Search empty tree
	{
		a.search(42);
		CPPUNIT_ASSERT(0);//You hit this, you screwed up
	}
	catch(Exception & e)//Throws exception
	{
		CPPUNIT_ASSERT(e.error_code()==CONTAINER_EMPTY);
	}
	
	a.insert(42);
	
	//We found it
	CPPUNIT_ASSERT(a.search(42)==42);
	
	try
	{
		a.search(99);//Search something that doesn't exist
		CPPUNIT_ASSERT(0);//You hit this, you screwed up
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code()==ITEM_NOT_FOUND);
	}
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_searchptr();
/// @brief This tests the searchptr function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the searchptr function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_searchptr()
{
	Bst <int> a;
	
	try//Search empty tree
	{
		a.search(42);
		CPPUNIT_ASSERT(0);//You hit this, you screwed up
	}
	catch(Exception & e)//Throws exception
	{
		CPPUNIT_ASSERT(e.error_code()==CONTAINER_EMPTY);
	}
	
	a.insert(42);
	
	//We found it
	CPPUNIT_ASSERT(a.searchptr(42)!=NULL);
	
	CPPUNIT_ASSERT(a.searchptr(99) == NULL);//Search something that doesn't exist
	
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_clear();
/// @brief This tests the clear function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the clear function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_clear()
{
	Bst <int> b;
	
	CPPUNIT_ASSERT(b.empty());//It's empty
	
	
	
	const int MAX = 5000;
	int array[MAX];

	//Fill Array
	for( int i = 0; i < MAX; i++)
	{
		array[i] = i;
	}

	int arraySwap;
	
	for(int i = 0; i < MAX; i++)
	{
		int k = rand()%MAX;
		arraySwap = array[i];
		array[i] = array[k];
		array[k] = arraySwap;		
	}
	
	for(int i = 0; i < MAX; i++)
	{
		b.insert(array[i]);
	}
	
	CPPUNIT_ASSERT(!b.empty());//It's not empty
	
	b.clear();
	
	CPPUNIT_ASSERT(b.empty());//It's empty again
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_empty();
/// @brief This tests the empty function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the empty function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_empty()
{
	Bst <int> a;
	
	CPPUNIT_ASSERT(a.empty());//It's empty
	
	a.insert(42);
	
	CPPUNIT_ASSERT(!a.empty());//It's not empty

	a.remove(42);
	
	CPPUNIT_ASSERT(a.empty());//It's empty again
	
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_size();
/// @brief This tests the size function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the size function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_size()
{
	Bst <int> a;
	
	for(int i = 0; i < 10; i++)
	{
		CPPUNIT_ASSERT(a.size()==i);
		a.insert(i);
		
	}
	
	for(int i = 9; i >= 0; i--)
	{
		CPPUNIT_ASSERT(a.size()==i+1);
		a.remove(i);
	}

}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_hammer();
/// @brief This just makes a huge BST and destroys it again.
/// @pre BST class should already be declared. 
/// This function will attempt to just crash the tree through brute force. 
/// Since it doesn't we can be reasonably confident that the BST is correct.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_bst::test_hammer()
{
	Bst<int> b;

  //run n iterations of this test
	for(int j = 0; j < 100; j++)
	{
		srand(j); //seed with a known value (So we can reproduce it)
		
		const int MAX = 100;
		int array[MAX];
		int b_current_size = 0; //Keep independent track of size

		//Fill Array
		for( int i = 0; i < MAX; i++)
		{
			array[i] = i;
		}

		int arraySwap;
		
		//Knuth shuffle
		for(int i = 0; i < MAX; i++)
		{
			int k = rand()%MAX;
			arraySwap = array[i];
			array[i] = array[k];
			array[k] = arraySwap;		
		}
		
		//Use the shuffled array to populate the tree
		for(int i = 0; i < MAX; i++)
		{
			b.insert(array[i]);
			b_current_size++;
			CPPUNIT_ASSERT(b.search(array[i]) == array[i]);
			CPPUNIT_ASSERT(b.size() == b_current_size);
		}
		
		b.clear();
		b_current_size = 0;//Reset what we are comparing to.
		
	}
	
	
}

