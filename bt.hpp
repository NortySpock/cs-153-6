//////////////////////////////////////////////////////////////////////////////
/// @file bt.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the bt implementation file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BT()
/// @brief The constructor for the binary tree
/// @pre No binary seach tree exists
/// @post A binary search tree exists, of size zero and with root pointing at
/// null
/// @param None (default constructor)
/// @return Nothing (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BT<generic>::BT()
{
  m_size = 0;
  m_root = NULL;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn ~BT()
/// @brief The destructor for the binary tree
/// @pre Binary search tree exists
/// @post Binary search tree does not exist
/// @param None (destructor)
/// @return None (destructor)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
BT<generic>::~BT()
{
  clear();
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BT(BT &)
/// @brief This is the copy constructor for the Binary Tree
/// @pre A binary tree exists
/// @post Two binary trees exist, the second being an exact clone of the first
/// @param Binary tree to be copied.
/// @return Binary tree
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
BT<generic>::BT (BT <generic> & x)
{
  m_size = 0;
  m_root = NULL;
  
  *this = x;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BT & operator= (const BT &);
/// @brief Defines the behavior of the assignment operator as applied to 
/// binary trees
/// @pre Two binary trees exist
/// @post The LHS is an exact duplicate of the RHS
/// @param LHS: assignee RHS: assigner
/// @return The assignee
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
BT<generic> & BT<generic>::operator= (const BT &rhs)
{
  //Gotta make a binary tree that is not dependent on BST format
  //"Dissen's gonna be messy! Me no watchen!" -- Jar Jar Binks

  clear();//Clear the current tree.
  
  //Nothing to copy
  if(rhs.m_root == NULL)//Can't use empty because it doesn't pass well.
  {
    //Done, nothing to copy.
  }
  else//Work our way through the tree to copy
  {
    Btn <generic> * copy_node = rhs.m_root; //The other iterator
    Btn <generic> * new_node; //The new tree iterator
    
    //Create the root node
    new_node = new Btn<generic>; 
    m_root = new_node; //Point m_root at where our new tree will be
    new_node -> data = new generic;
    *(new_node -> data) = *(copy_node -> data); //Copy data
    //First node goes nowhere
    new_node -> p = NULL;
    new_node -> l = NULL;
    new_node -> r = NULL;
    
    //So long as the iterator for the copy hasn't bailed out the top.
    while(copy_node != NULL)
    {
      //We're going to step through each node of the tree in sync
      
      //If copy goes left and new has not yet
      if(copy_node -> l != NULL && new_node -> l == NULL)
      {
        //Create new node for new side
        new_node -> l = new Btn <generic>;
        new_node -> l -> data = new generic;
        new_node -> l -> p = new_node;//Point new node back to parent
        new_node -> l -> l = NULL;//leaf node has no children
        new_node -> l -> r = NULL;
        *(new_node -> l -> data) = *(copy_node -> l -> data); //Copy data
        //Go left in step
        copy_node = copy_node -> l;
        new_node = new_node -> l;
      }
      //If copy goes right and new has not yet
      else if(copy_node -> r != NULL && new_node -> r == NULL)
      {
        //Create new node for new side
        new_node -> r = new Btn <generic>;
        new_node -> r -> data = new generic;
        new_node -> r -> p = new_node;//Point new node back to parent
        new_node -> r -> l = NULL;//leaf node has no children
        new_node -> r -> r = NULL;
        *(new_node -> r -> data) = *(copy_node -> r -> data); //Copy data
        //Go right in step
        copy_node = copy_node -> r;
        new_node = new_node -> r;
      }
      //We hit the bottom of the copy, so go up
      else
      {
        copy_node = copy_node -> p;
        new_node = new_node -> p;
      }//else go up
    }//While not out the top
  }//else go through the rest of the tree

  return * this;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void clear()
/// @brief Clears the tree so that it contains nothing.
/// @pre Tree exists
/// @post Tree is empty, with root pointing to null
/// @param None
/// @return Nothing (void)
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
void BT<generic>::clear()
{
  Btn<generic> * step = m_root;
  Btn<generic> * deleteNode;

  //Get to leftmost leaf node
  if(step != NULL)
  {
    while(step -> l != NULL || step -> r != NULL)
    {
      while(step -> l != NULL)
      {
        step = step -> l;
      }
      if(step -> r != NULL)
      {
        step = step -> r;
      }
    }
  }
  //while not done
  while(m_root != NULL)
  {
    if(step != NULL)
    {
      if(step -> p != NULL)
      {
        //We're left child
        if(step -> p -> l == step)
        {
          deleteNode = step; //mark for deletion
          step = step -> p; //step up
          step -> l = NULL; //cut link
          
          delete deleteNode -> data;
          delete deleteNode;
          m_size--;
          
          //Now that we've stepped up from left, can we go right?
          if(step -> r != NULL)
          {
            step = step -> r;
            //Go to that leaf node
            while(step -> l != NULL || step -> r != NULL)
            {
              while(step -> l != NULL)
              {
                step = step -> l;
              }//while can go left
              if(step -> r != NULL)
              {
                step = step -> r;
              }//if can go right
            }//while not at leaf node
          }//if we could go right after going up one.
        }//if left child leaf node
        else//We're the right child
        {
          deleteNode = step; //Mark for deletion
          step = step -> p; //step up
          step -> r = NULL; //cut link
          
          delete deleteNode -> data;
          delete deleteNode;
          m_size--;
        }//right child  
      }//if parent not null
      else//We're at root
      {
        deleteNode = step;
        step = m_root = NULL;
        
        delete deleteNode -> data;
        delete deleteNode;
        m_size--;
      }//else at root
    }//if current not null
  }//while not done
  
}//Clear function

//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty()
/// @brief Tests to see if the tree is empty (m_root is null)
/// @pre Tree exists
/// @post No change to tree
/// @param None
/// @return TRUE if tree is empty, FALSE if it is not
////////////////////////////////////////////////////////////////////////////// 
template <class generic>
bool BT<generic>::empty()
{
  if(m_root == NULL)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size ()
/// @brief Returns the current number of elements in the tree
/// @pre Tree exists
/// @post No change
/// @param None
/// @return Number of elements in the tree.
//////////////////////////////////////////////////////////////////////////////  
template <class generic>
unsigned int BT<generic>::size()
{
  return m_size;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn PreOrder pre_begin () const;
/// @brief Returns the first node the iterator should touch.
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Position of the first node.
//////////////////////////////////////////////////////////////////////////////
//Start at root
template <class generic>
BTPreorderIterator<generic> BT<generic>::pre_begin() const
{
  if(m_root == NULL)
  {
    throw Exception(CONTAINER_EMPTY, "The tree is empty.");
  }
  return PreOrder(m_root);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn PreOrder pre_end () const;
/// @brief Returns the last position the iterator should be at (NULL)
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Last position the iterator should be at (NULL)
//////////////////////////////////////////////////////////////////////////////
//End at right-most node.
template <class generic>
BTPreorderIterator<generic> BT<generic>::pre_end() const
{
  Btn<generic> * temp = NULL;  
  return PreOrder(temp);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn InOrder in_begin () const;
/// @brief Returns the first node the iterator should touch.
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Position of the first node.
//////////////////////////////////////////////////////////////////////////////
//Start at left-most node
template <class generic>
BTInorderIterator<generic> BT<generic>::in_begin() const
{
    Btn<generic>* temp = m_root;
    if (temp != NULL)//We don't try something dumb
    {
      //Go left as far as possible
      while(temp -> l != NULL)
      {
        temp = temp -> l;
      }
    }    
    else
    {
      throw Exception(CONTAINER_EMPTY, "The tree is empty.");
    }
    return InOrder(temp);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn InOrder in_end () const;
/// @brief Returns the last position the iterator should be at (NULL)
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Last position the iterator should be at (NULL)
//////////////////////////////////////////////////////////////////////////////
//End at right-most node
template <class generic>
BTInorderIterator<generic> BT<generic>::in_end() const
{
  Btn<generic>* temp = NULL;
  return InOrder(temp);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn PostOrder post_begin () const;
/// @brief Returns the first node the iterator should touch.
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Position of the first node.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPostorderIterator<generic> BT<generic>::post_begin() const
{
  Btn<generic>* temp = m_root;
  
  if(temp != NULL)//We don't try something dumb
  {
    while(temp -> l != NULL || temp -> r != NULL)
    {
      while(temp -> l != NULL)
      {
        temp = temp -> l;
      }
    
      if(temp -> r != NULL)
      {
        temp = temp -> r;
      }//if
    }//while
  }//if not dumb
  else
  {
     throw Exception(CONTAINER_EMPTY, "The tree is empty.");
  }
  return PostOrder(temp);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn PostOrder post_end () const;
/// @brief Returns the last position the iterator should be at (NULL)
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Last position the iterator should be at (NULL)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPostorderIterator<generic> BT<generic>::post_end() const
{
  Btn<generic>* temp = NULL;
  return PostOrder(temp);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void swap (Btn<generic> *, Btn<generic> *);
/// @brief Swaps two elements of data.
/// @pre Tree exists with at least two elements in it
/// @post Two elements data is swapped.
/// @param None
/// @return none
//////////////////////////////////////////////////////////////////////////////
template <class generic>
void swap (Btn <generic> * first, Btn <generic> * second)
{
  generic * temp;
  
  temp = first -> data;
  first -> data = second -> data;
  second -> data = temp;
}

