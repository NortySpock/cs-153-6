//////////////////////////////////////////////////////////////////////////////
/// @file btpostorderiterator.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the post-order iterator implementation file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPostorderIterator ()
/// @brief Post-order iterator constructor.
/// @pre Post-order iterator does not exist
/// @post Post-order iterator exists with iterator position not set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPostorderIterator<generic>::BTPostorderIterator ()
{
  m_current = NULL;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPostorderIterator (Btn<generic> *)
/// @brief Post-order iterator constructor.
/// @pre Post-order iterator does not exist
/// @post In-order iterator exists with iterator position already set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPostorderIterator<generic>::BTPostorderIterator (Btn<generic> * x) : m_current (x)
{
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Dereference operator for Post-order iterator.
/// @pre Iterator is pointing at a non-NULL node of tree.
/// @post Iterator is pointing at a non-NULL node of tree.
/// @param None.
/// @return Data pointed to by node of tree.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic BTPostorderIterator<generic>::operator* () const
{
  return *(m_current -> data);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPostorderIterator operator++ ()
/// @brief Iterate operator for iterator. Steps one step through the tree.
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPostorderIterator<generic> BTPostorderIterator<generic>::operator++ ()
{
    //So long as we're not trying to do something stupid
  if(m_current != NULL)
  {
    if(m_current -> p != NULL)//We're not at root
    {
      if(m_current -> p -> l == m_current)//we're left child
      {
        m_current = m_current -> p;//Go up one
        
        if(m_current -> r != NULL)//We can go right
        {
          m_current = m_current -> r; //go right
          //Now go left-rightmost
          while(m_current -> l != NULL || m_current -> r != NULL)
          {
            while(m_current -> l != NULL)
              {
                m_current = m_current -> l;
              }
    
              if(m_current -> r != NULL)
              {
                m_current = m_current -> r;
              }//if
          }//while
        }//If can go right
        
        //else we couldn't go right, so we already just went up one.
        
      }//if left child
      else//right child
      {
        m_current = m_current -> p;//just go up one
      }
    }//if not at root
    else//We're at root, done. Set m_current to null to indicate doneness.
    {
      m_current = NULL;
    } 
  }
  //Else we tried to do something stupid, do nothing.
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPostorderIterator operator++ (int)
/// @brief Altered version of the normal ++ operator to handle preincrements
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPostorderIterator<generic> BTPostorderIterator<generic>::operator++ (int)
{
  return ++(*this);//Apparently this is an ugly hack for the pre-inc-operator
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const BTPostorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: true if iterators are pointing at the same thing, false if not
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool BTPostorderIterator<generic>::operator== (const BTPostorderIterator & x) const
{
  return m_current == x.m_current;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const BTPostorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: false if iterators are pointing at the same thing, true if not
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool BTPostorderIterator<generic>::operator!= (const BTPostorderIterator &x) const
{
  return m_current != x.m_current;
}

