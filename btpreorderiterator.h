//////////////////////////////////////////////////////////////////////////////
/// @file btpreorderiterator.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the pre-order iterator header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class btpreorderiterator
/// @brief The preorder iterator for a binary tree.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator ()
/// @brief Pre-order iterator constructor.
/// @pre Pre-order iterator does not exist
/// @post Pre-order iterator exists with iterator position not set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator (Btn<generic> *)
/// @brief Pre-order iterator constructor.
/// @pre Pre-order iterator does not exist
/// @post Pre-order iterator exists with iterator position already set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Dereference operator for Pre-order iterator.
/// @pre Iterator is pointing at a non-NULL node of tree.
/// @post Iterator is pointing at a non-NULL node of tree.
/// @param None.
/// @return Data pointed to by node of tree.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator operator++ ()
/// @brief Iterate operator for iterator. Steps one step through the tree.
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator operator++ (int)
/// @brief Altered version of the normal ++ operator to handle preincrements
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const BTPreorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: true if iterators are pointing at the same thing, false if not
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const BTPreorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: false if iterators are pointing at the same thing, true if not
//////////////////////////////////////////////////////////////////////////////

#ifndef BTPREORDERITERATOR_H
#define BTPREORDERITERATOR_H

#include "btn.h"

template <class generic>
class BTPreorderIterator
{
  public:
    BTPreorderIterator ();
    BTPreorderIterator (Btn<generic> *);
    generic operator* () const;
    BTPreorderIterator operator++ ();
    BTPreorderIterator operator++ (int);
    bool operator== (const BTPreorderIterator &) const;
    bool operator!= (const BTPreorderIterator &) const;
    
    private:
    Btn<generic> * m_current;//The node the iterator is currently pointing at  
};

#include "btpreorderiterator.hpp"
#endif

