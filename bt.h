//////////////////////////////////////////////////////////////////////////////
/// @file bt.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the bt header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class Bt
/// @brief The Binary Tree, from which other, more compled trees inheirit
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BT()
/// @brief The constructor for the binary tree
/// @pre No binary seach tree exists
/// @post A binary search tree exists, of size zero and with root pointing at
/// null
/// @param None (default constructor)
/// @return Nothing (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn ~BT()
/// @brief The destructor for the binary tree
/// @pre Binary search tree exists
/// @post Binary search tree does not exist
/// @param None (destructor)
/// @return None (destructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn BT(BT &)
/// @brief This is the copy constructor for the Binary Tree
/// @pre A binary tree exists
/// @post Two binary trees exist, the second being an exact clone of the first
/// @param Binary tree to be copied.
/// @return Binary tree
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn BT & operator= (const BT &);
/// @brief Defines the behavior of the assignment operator as applied to 
/// binary trees
/// @pre Two binary trees exist
/// @post The LHS is an exact duplicate of the RHS
/// @param LHS: assignee RHS: assigner
/// @return The assignee
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void clear()
/// @brief Clears the tree so that it contains nothing.
/// @pre Tree exists
/// @post Tree is empty, with root pointing to null
/// @param None
/// @return Nothing (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty()
/// @brief Tests to see if the tree is empty (m_root is null)
/// @pre Tree exists
/// @post No change to tree
/// @param None
/// @return TRUE if tree is empty, FALSE if it is not
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size ()
/// @brief Returns the current number of elements in the tree
/// @pre Tree exists
/// @post No change
/// @param None
/// @return Number of elements in the tree.
//////////////////////////////////////////////////////////////////////////////  

//////////////////////////////////////////////////////////////////////////////
/// @fn PreOrder pre_begin () const;
/// @brief Returns the first node the iterator should touch.
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Position of the first node.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn PreOrder pre_end () const;
/// @brief Returns the last position the iterator should be at (NULL)
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Last position the iterator should be at (NULL)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn InOrder in_begin () const;
/// @brief Returns the first node the iterator should touch.
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Position of the first node.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn InOrder in_end () const;
/// @brief Returns the last position the iterator should be at (NULL)
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Last position the iterator should be at (NULL)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn PostOrder post_begin () const;
/// @brief Returns the first node the iterator should touch.
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Position of the first node.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn PostOrder post_end () const;
/// @brief Returns the last position the iterator should be at (NULL)
/// @pre Tree and iterator exist
/// @post None
/// @param None
/// @return Last position the iterator should be at (NULL)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void swap (Btn<generic> *, Btn<generic> *);
/// @brief Swaps two elements of data.
/// @pre Tree exists with at least two elements in it
/// @post Two elements data is swapped.
/// @param None
/// @return none
//////////////////////////////////////////////////////////////////////////////

#ifndef BT_H
#define BT_H

#include "btn.h"
#include "exception.h"
#include "btpreorderiterator.h"
#include "btinorderiterator.h"
#include "btpostorderiterator.h"

template <class generic>
class BT
{
  public:
    BT();
    ~BT();
    BT(BT &);
    BT & operator= (const BT &);
    void clear ();
    bool empty ();
    unsigned int size ();

    typedef BTPreorderIterator<generic> PreOrder;
    typedef BTInorderIterator<generic> InOrder;
    typedef BTPostorderIterator<generic> PostOrder;
    PreOrder pre_begin () const;
    PreOrder pre_end () const;
    InOrder in_begin () const;
    InOrder in_end () const;
    PostOrder post_begin () const;
    PostOrder post_end () const;

  protected:
    Btn<generic> * m_root;
    unsigned int m_size;
    void swap (Btn<generic> *, Btn<generic> *);
};

#include "bt.hpp"
#endif

