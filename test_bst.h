//////////////////////////////////////////////////////////////////////////////
/// @file test_bst.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header file for the test of the BST class.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class test_bst
/// @brief This is the header file for the class that tests the BST
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST_SUITE (Test_bst);
/// @brief This calls the unit testing library for CPP, 
/// using this header file as an argument
/// @pre Requires the indicated header file to exist 
/// @post Reports the success or failure of unit tests.
/// @param Test_stack is the header being called
/// @return I believe this is void, instead cout-ing 
/// the results to the terminal
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_constructor);
/// @brief This calls the unit testing library for CPP, 
/// using the test_constructor member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_constructor is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_insert);
/// @brief This calls the unit testing library for CPP, 
/// using the test_insert member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_insert is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_remove);
/// @brief This calls the unit testing library for CPP, 
/// using the test_remove member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_remove is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_search);
/// @brief This calls the unit testing library for CPP, 
/// using the test_search member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_search is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_searchptr);
/// @brief This calls the unit testing library for CPP, 
/// using the test_searchptr member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_searchptr is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_clear);
/// @brief This calls the unit testing library for CPP, 
/// using the test_clear member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_clear is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_empty);
/// @brief This calls the unit testing library for CPP, 
/// using the test_empty member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_empty is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_size);
/// @brief This calls the unit testing library for CPP, 
/// using the test_size member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_size is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_hammer);
/// @brief This calls the unit testing library for CPP, 
/// using the test_hammer member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_hammer is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor();
/// @brief This tests the constructor of the BST class
/// @pre BST class should already be declared. 
/// This function will instantiate a BST and make sure the initial 
/// values are correct (size is zero, m_root is null).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_insert();
/// @brief This tests the insert function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the insert function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_remove();
/// @brief This tests the remove function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the remove function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_search();
/// @brief This tests the search function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the search function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_searchptr();
/// @brief This tests the searchptr function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the searchptr function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_clear();
/// @brief This tests the clear function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the clear function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_empty();
/// @brief This tests the empty function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the empty function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_size();
/// @brief This tests the size function of the BST class
/// @pre BST class should already be declared. 
/// This function will attempt to ensure that the size function
/// works properly.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_hammer();
/// @brief This just makes a huge BST and destroys it again.
/// @pre BST class should already be declared. 
/// This function will attempt to just crash the tree through brute force. 
/// Since it doesn't we can be reasonably confident that the BST is correct.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

#ifndef TEST_BST_H
#define TEST_BST_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "bst.h"


class Test_bst : public CPPUNIT_NS::TestFixture
{
  private:
  CPPUNIT_TEST_SUITE (Test_bst);

    CPPUNIT_TEST (test_constructor);
    CPPUNIT_TEST (test_insert);
    CPPUNIT_TEST (test_remove);
    CPPUNIT_TEST (test_search);
    CPPUNIT_TEST (test_searchptr);
    CPPUNIT_TEST (test_clear);
    CPPUNIT_TEST (test_empty);
    CPPUNIT_TEST (test_size);

    CPPUNIT_TEST (test_hammer);    
  CPPUNIT_TEST_SUITE_END ();

  protected:
  void test_constructor();
  void test_insert();
  void test_remove();
  void test_search();
  void test_searchptr();
  void test_clear();
  void test_empty();
  void test_size();  
  void test_hammer();
};

#endif

